import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  private genders = [];
  private time = [];
  private bottles = [];
  private weight : number;
  private bottle : number;
  promilles : number;
  private times : number;
  private gender : string;
  private burning : number;
  private grams : number;
  private litres : number;
  gramsLeft : number;


  constructor() {}

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    this.time.push(1);
    this.time.push(2);
    this.time.push(3);


    this.bottles.push(1);
    this.bottles.push(2);
    this.bottles.push(3);



    this.gender = 'Male';
    this.times = 1;
    this.bottle = 1;
    }

  private calculate() {

    this.litres = this.bottle * 0.33;
    this.grams = this.litres * 8 * 4.5;
    this.burning = this.weight / 10;
    this.gramsLeft = this.grams - (this.burning * this.times);

    if (this.gender === 'Male') {
      this.promilles = this.gramsLeft / (this.weight * 0.7);
      }
    else {
      this.promilles = this.gramsLeft / (this.weight * 0.6);
      }
  }
}

